# SVTAssesment




## Prompt

One of SVT's microservices calculates which robot should transport a pallet from point A to point B based on which robot is the closest and has the most battery left if there are multiple in the proximity of the load's location. You'll use a provided API endpoint to create a simplified robot routing API.

This is the endpoint to retrieve a list of robots in our robot fleet: https://60c8ed887dafc90017ffbd56.mockapi.io/robots.

The provided API returns a list of all 100 robots in our fleet. It gives their current position on an xy-plane along with their battery life. Your job is to write a new .NET Core API with an endpoint which accepts a payload with a load which needs to be moved including its identifier and current x,y coordinates and your endpoint should make an HTTP request to the robots endpoint and return which robot is the best to transport the load based on which one is closest the load's location. If there is more than 1 robot within 10 distance units of the load, return the one with the most battery remaining.

## Starting the application

- run tests
```
dotnet test
```

- start application
```
dotnet run --project RobotService
```

The application will run on `https://localhost:5001`. Since this is just an api reaching that page directly will be blank. Instead try `https://localhost:5001/swagger/index.html` to use the API through SwaggerUI. Alternatively you can use a API test suite like Postman.

## Making a request

Template request body:
```
{
  "loadId": 0,
  "x": 0,
  "y": 0
}
```

If you think of the floor as a grid then `x` is the horizontal position and `y` is the vertical position of the load specified by `loadId`.
The api will check the list of robots for the closest robot to the load. If there are more robots less than 10 distance units to the load then the robot with the most battery level is returned.

## TODOs

- Right now all I am doing is writing the message to console no matter the exception. In the future it would be wise to handle each independently.
- Not just select the closest robot or the robot with the most battery, but the closest robot AND with the most battery level. This would require creating a model to determine the closest robot with the most battery. Since the closest robot or robot with most battery satisfies the prompt this TODO is marked as a parity feature.
- Add intergration testing to better test the cache and the applications ability to withstand overflows.
