using System;
using Xunit;
using RobotService.Controllers;
using RobotService.DTOs;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace RobotService.Tests
{
    public class RobotServiceTest
    {
        private readonly Robotics controller;
        public RobotServiceTest() 
        {
            var mockLogger = new Mock<ILogger<Robotics>>();

            var services = new ServiceCollection();
            services.AddMemoryCache();
            var serviceProvider = services.BuildServiceProvider();
            var memoryCache = serviceProvider.GetService<IMemoryCache>();
            
            controller = new Robotics(mockLogger.Object, memoryCache);
        }
        
        [Fact]
        public async Task Works()
        {
            var testRequest = new GetRobotRequest 
            {
                LoadId = 1,
                X = new Random().Next( int.MinValue, int.MaxValue),
                Y = new Random().Next( int.MinValue, int.MaxValue)
            };
            var res = await controller.GetRobot(testRequest);
            Assert.IsType<OkObjectResult>(res);
        }

        // TODO: Add intergration testing to better test the cache and the applications ability to withstand overflows.
    }

}
