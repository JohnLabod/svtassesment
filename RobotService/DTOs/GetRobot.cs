using System.ComponentModel.DataAnnotations;

namespace RobotService.DTOs
{
    public class GetRobotRequest
    {
        [Range(1, int.MaxValue)]
        public int LoadId {get; set;}
        [Required]
        public int? X {get; set;}
        [Required]
        public int? Y {get; set;}
    }

    public class GetRobotResponse
    {
        public string RobotId {get; set;}
        public double DistanceToGoal {get; set;}
        public int BatteryLevel {get; set;}
    }

}