namespace RobotService.DTOs
{
    public class GetFleet 
    {
        public string RobotId {get; set;}
        public int BatteryLevel {get; set;}
        public int X {get; set;}
        public int Y {get; set;}
        public double DistanceToGoal {get; set;}
    }
}