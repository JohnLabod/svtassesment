﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Text.Json;
using RobotService.DTOs;
using Microsoft.Extensions.Caching.Memory;
using System.Net.Http.Json;

namespace RobotService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class Robotics : ControllerBase
    {
        private readonly ILogger<Robotics> _logger;
        private readonly IMemoryCache _cache;
        private static readonly HttpClient client = new HttpClient();
        private static readonly string _fleetEntry = "_FleetEntry";
        public Robotics(ILogger<Robotics> logger, IMemoryCache cache)
        {
            _logger = logger;
            _cache = cache;
        }

        [HttpPost]
        public async Task<IActionResult> GetRobot(GetRobotRequest loadInformation)
        {
            try
            {
                var cacheAcceptable = _cache.TryGetValue(_fleetEntry, out ICollection<GetFleet> robotFleet);
                if (!cacheAcceptable)
                {
                    robotFleet = await getRobotFleet();
                    // Cache the response from the external endpoint for one minutes
                    // One minutes was chosen as the cache limit in an arbitrary fashion.
                    // With any new information the cache limit could be set differently
                    _cache.Set(_fleetEntry,
                        robotFleet,
                        new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(1)));
                }
                var robotsWithinDistance = false;
                GetFleet closestRobot = null;
                GetFleet highestBatteryLevelRobot = null;
                var minimumDistance = 10;
                foreach (var robot in robotFleet)
                {
                    robot.DistanceToGoal = distanceBetweenTwoPoints((int)loadInformation.X, robot.X, (int)loadInformation.Y, robot.Y);
                    if (robot.DistanceToGoal <= minimumDistance)
                    {
                        robotsWithinDistance = true;
                        if(highestBatteryLevelRobot == null || robot.BatteryLevel > highestBatteryLevelRobot.BatteryLevel)
                        {
                            highestBatteryLevelRobot = robot;
                        }
                    }
                    if(closestRobot == null || robot.DistanceToGoal < closestRobot.DistanceToGoal)
                    {
                        closestRobot = robot;
                    }
                }
                // If there are robots within the minimum distance units then take the one with the biggest battery 
                GetFleet robotToReturn;
                if(robotsWithinDistance)
                {
                    robotToReturn = highestBatteryLevelRobot;
                } 
                // If there are no robots within the minimum distance units of the goal, then return the closest robot
                else
                {
                    robotToReturn = closestRobot;
                }
                // TODO: Not just select the closest robot or the robot with the most battery, but the closest robot AND with the most battery level.
                //       This would require creating a model to determine the closest robot with the most battery.
                //       Since the closest robot or robot with most battery satisfies the prompt this TODO is marked as a parity feature.

                return Ok(new GetRobotResponse {
                    RobotId = robotToReturn.RobotId,
                    BatteryLevel = robotToReturn.BatteryLevel,
                    DistanceToGoal = robotToReturn.DistanceToGoal
                });
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return StatusCode(500);
            }
        }

        private async Task<ICollection<GetFleet>> getRobotFleet()
        {
            try
            {
                var url = "https://60c8ed887dafc90017ffbd56.mockapi.io/robots";
                var fleet = await client.GetFromJsonAsync<ICollection<GetFleet>>(url);
                
                return fleet;
            }
            //TODO: Right now all I am doing is writing the message to console no matter the exception. In the future it would be wise to handle each independently.
            catch (HttpRequestException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            catch (JsonException e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
            
        }

        private double distanceBetweenTwoPoints(int x1, int x2, int y1, int y2)
        {
            return Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        }
    }
}
